# Proposed File / Directory Structure for Projects

This document describes a proposed directory structure applicable to
projects. The main features of this structure are:

## Main features

-   Separation of Project organization and project result

    -   providing 24_sources as a separate area for project results.
    -   This area is self contained and can easily be transferred to
        another project organization.

-   Applicable for Multi-WorkGroup-Projects
    -   Workgroup specific folders

-   Applicable to multiple platforms
    -   only 7bit ascii characters
    -   Case sensitive names

-   Easy handling even in Telephone
    -   unique numbering scheme
    -   distinctive filenames

## Document naming scheme

The general document naming scheme is provided as

    {Class}_{ShortName}[_{id}]

with

-   `{class}` - The classification of the document basically it is one
    of

    -   Project management document

        -   `EXP` - Explanation Document
        -   `PD` - Process Description
        -   `PM` - a project manual

    -   Project result documents

    -   `CONC` - Concept Document (L3)
    -   `RS` - Requirement Specification (L4)
    -   `ISP` - Implementation Specification (L5)
    -   `EUD` - End user Documentation (L5)
    -   `TS` - TestSpecification
    -   `TPL` - Template
    -   `. . .` - Further document classes on request

    -   Auxillary Documents and Support material

    -   `ZSUPP` - Supporting material
    -   `ZGEN` - Generatoed material - do not edit files here

-   `{ShortName}` the short name of the document according to the master
    Document list (`PD_MasterDocumentList`), which may also be
    represented as a page in the ProjectBook

-   `{Id}` a numerical project id, if the project has decided to use
    those.

## Requirements tracing

Requirements tracing acts between the levels indicated at the document
classes above.

## Directory layout

-   `01_GeneralOrganization`

    This folder contains arbitrary information about the project. This
    is basically Information to be provided for advertisement etc.

-   `03_ProjectManagement`

    This folder contains the current project management document
    structure. Basically it is the Projectplan (roadmap) and the project
    repairing.

-   `10_WorkingGroups`

    This is the folder for the working groups

    -   `01_Wp_xx`
        -   `01_ProjectBook`

            This is the project book

        -   `02_Meetings`

            This is for meeting organization

        -   `06_InfoSources`

            This is the place to store all external information sources

        -   `90_Scratchpad`

            This is the area for playing around

            -   `01_Bernhard`
            -   `01_Carolin`

-   `20_Sources`

    -   if multiple branches are maintained simultaneously, they should
        be handled as Subdirectories here.

    -   `. . .  one directory per document . . .`

