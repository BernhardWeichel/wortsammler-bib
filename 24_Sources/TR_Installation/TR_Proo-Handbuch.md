# Benutzerhandbuch

Dies ist ein erster Wurf für das Benutzerhandbuch

## Versionsverwaltung und Ablage

### Erstellen einer lokalen Arbeitskopie

Zunächst muss die Arbeitsumgebung von der zentralen Ablage geholt
werden.

![svn-checkout-menu][] 

dann erscheint dieser Dialog.

![svn-checkout-dialog][] 

Die so entstehende Arbeistkopie liegt lokal auf dem Rechner und kann
immer wieder verwendet werden

### Ein typischer Bearbeitungszyklus

1.  Bevor man mit der Arbeit beginnt, muss man die Arbeitskopie
    aktualisieren. Das geschieht mit `SVN Update`

2.  Dann kann man mit der normalen Bearbeitung beginnen.

    Den Änderungszustand der Dateien kann man im Explorer sehen.

    ![svn-icons][] 

3.  Wenn man fertig ist, muss man die Arbeit zurückgeben.

    ![svn-commit-menu][] 

    Dann kommt dieser Dialog.

    ![svn-commit-dialog][] 

    Hier sollte man einen sinnvollen Kommentar eingeben, der die
    Änderungen charakterisiert. Dieser Kommentar kann von den anderen
    Team-Mitgliedern gesehen werden.

4.  Das Ergebnis dieses Vorgangs sieht man hier:

    ![svn-commit-result][] 

### Hinzufügen neuer Dateien

TODO: kommt noch

Menü:`Tortoise SVN/Add`

### Umbenennen von Dateien

TODO: kommt noch

Menü:`Tortoise SVN/rename`

## Bearbeitung der Textbausteine

Die Textbausteine werden im Format `Markdown` bearbeitet.

Die Textbausteine können mit einem beliebigen Texteditor, (zum Beispiel
`Notepad++`) bearbeitet werden. `Markdownpad` bietet eine spezifische
Unterstützung für Markdown und eine laufend mitgeführte Vorschau.

## Aufbereitung der Dokumente

TODO: … kommt noch …

## Konfiguration der Ausgabedokumente (Manifest)

<a id="manifest"/> TODO: … kommt noch ...

# Markdown Syntax

`Markdown` ist eine vereinfachte Auszeichnungssprache. Ein Ziel von
Markdown ist, dass schon die Ausgangsform ohne weitere Konvertierung
leicht lesbar ist. Als Auszeichnungselemente wurden daher vor allem
Auszeichnungsarten verwendet, die bei der Schreibmaschine und und
E-Mails üblich sind.

Wortsammler verarbeitet markdown mit Hilfe von [Pandoc][] bzw.
[LaTeX][]. Für spezifesche Anforderungen gibt es weiterführende
Konventionen, die von Wortsammler verarbeitet werden:

-   Tracing
-   Einbindung von PDF-Seiten
-   Textschnipsel
-   Dokumentinterne Verweise
-   Zielgruppespezifische Ausgaben

## Erzeugen von Kapiteln und Überschriften

Überschriften entstehen durch voranstellen von "\#"

    # Header 1

    ## Header 2

    ### Header 3

Dieses Kapitel entsteht zum Beispiel durch

    ### Überschriften

    Überschriften entstehen durch voranstellen von "#"

## Paragraphen und Zeilenumbrüche

-   Ein Paragraph entsteht durch eine oder mehrere aufeinander folgende
    Textteilen, getrennt durch eine leerzeile.
-   Eine Zeile, die mit einem Backslash (`\`) erzeugt einen manuellen
    Zeilenumbruch.

## Kursiv, Fettdruck, Hochstellung, Tiefstellung, Durchstreichung

-   *kursiv* entsteht durch `*kursiv*`
-   **fettdruck** entsteht durch `**fettdruck**`
-   ~~durchstreichen~~ entsteht durch `~~durchstreichen~~`
-   Hoch^stellung^ entsteht durch `Hoch^stellung^`
-   Tief~stellung~ entsteht durch `Tief~stellung~`
-   `Schreibmaschinenschrift` entsteht durch einfache 'backticks'
    `` `Schreibmaschinenschrift` ``

    Wenn der verbatim text ein back tick enthält, sollte man es in
    doppelte backticks einhüllen.

        `Schreibmaschinenschrift` entsteht durch einfache 'backticks'
        `` `Schreibmaschinenschrift` ``

-   Leerzeichen innerhalb von Texthervorhebung müssen durch `\`
    geschützt werden, zum Beispiel e.g., H~this is a long subscript~
    `H~this\ is\ a\ long\ subscript~`.

## Numerierte Aufzählungen

Numerierte Aufzählungen (auch geordnete Listen genannt) entstehen durch
manuelles voranstellen einer Aufzählungsnummer. Es kann bei allen
Einträgen die gleiche Nummer verwendet werden. Wortsammler baut aber die
richtige Nummer ein.

Aus

    1. erster Eintrag
    1. zweiter Eintrag

entsteht

1.  erster Eintrag
2.  zweiter Eintrag

~~ED detail~~

aus

    A)  erster Eintrag
    A)  zweiter Eintrag

entsteht

A)  erster Eintrag
B)  zweiter Eintrag

aus

    a)  erster Eintrag
    a)  zweiter Eintrag

entsteht

a)  erster Eintrag
b)  zweiter Eintrag

~~ED all~~

## Einfache Aufzählungen (Listen)

Einfache Aufzählungen (ungeordnete Listen) entstehen durch markieren der
Einträge mit '\*', '+', oder '-' markiert werden. ~~SN Wortsammler~~
ersetzt diese aber immer durch "-" in den Eingabedateien.

aus

    +   example
    -   example
    *   example

entsteht

-   example
-   example
-   example

## Verschachtelte Aufzählungen

Listen können durch Einrücken um vier Zeichen verschachtelt werden:

Aus

    -   example
        -   Das ist die innere liste. Sie kann auch aus mehreren
            Paragraphen bestehen.

            Das ist der zweite Paragraph der inneren Liste

        -   Das ist der zweite Eintrag der inneren Liste

    -   example

entsteht

-   example
    -   Das ist die innere liste. Sie kann auch aus mehreren Paragraphen
        bestehen.

        Das ist der zweite Paragraph der inneren Liste

    -   Das ist der zweite Eintrag der inneren Liste

-   example

## Definitionslisten

Definitionslisten beschreiben einen Begriff und dessen Definition.

Aus

    Term 1
    :   Definition 1
    Term 2
    :   Definition 2

        Zweiter parargraph von  Definition 2.

entsteht

Term 1
:   Definition 1

Term 2
:   Definition 2

    Zweiter parargraph von Definition 2.

## Einrückungen (Zitate)

Zitate - meist eingerückt dargestellt - entstehen durch ein
vorangestelltes `>` (eben wie bei emails)

aus

    >   das ist ein Zitat
    >   das auch aus mehreren Zeilen bestehen kann
    >
    >   und noch ein Absatz im Zitat
    >
    >>  verschachteltes Zitat
    >>  nested blockquote

entsteht

> das ist ein Zitat das auch aus mehreren Zeilen bestehen kann
>
> und noch ein Absatz im Zitat
>
> > verschachteltes Zitat nested blockquote

## Dokumentinterne Verweise (Wortsammler)

Für die Handhabung von dokument internen verweise braucht man die
folgenden **beiden** markierungen:

1.  Verweisziel

    Das Verweisziel entsteht durch die spezifische Eingabe (eingebettete
    HTML - Syntax): `<a id="name des Verweisziels">` (Wortsammler)

    Wird Requirements tracing verwendet, werden aus den Trace-ids
    ebenfalls Verweisziele erzeugt.

2.  Verweis

    Der Verweis selbst wird ebenfalls als HTML erfasst:
    `<a href="id">text für den Verweis</a>`.

    Verweise auf Trace-Items werden als `->[id]` markiert.

## Sonstige Verweise (pandoc)

### Implizite verweise auf emails und Webseiten

aus

    <http://example.com>
    <foo@bar.com>

entsteht

<http://example.com> \
<foo@bar.com>

### direkte Verweise

Bei expliziten verweisen steht der Text für das Dokument in eckigen
Klammern, gefolgt vom Verweisziel in runden Klammern.

    [inline link](http://example.com "Title")

### indirekte Verweise

Bei indirekten Verweisen wird das Verweisziel an andrer Stelle im
Dokument einmalig definiert und ggf. mehrfach verwendet. Folgende
Eingabe ist ein indirekter Verweis.

-   `[reference link][id]` - id ist er name des Verweisziels
-   `[implicit reference link][]` oder `[implicit reference link]` Der
    Name des Verweisziels fällt mit dem darzustellenden Text zusammen.

Die Verweisziele können an einer Stelle im Dokument detailliert werden.

    [id]: http://example.com "Title"
    [implicit reference link]: http://example.com
    [id2]: /path/to/image "alt text"

## eingebettete Grafiken

Ein vorangestelltes Ausrufezeichen zeigt an, dass das Verweisziel eine
einzubettende Grafik markiert.

        ![inline image](/path/to/image, "alt text") 
        ![reference image][id2]

**Hinweis**: Wenn eine Grafik alleine in einem Paragraphen steht, dann
wird sie nummeriert und ggf. auch innerhalb des Textes um die Seiten
optimal zu füllen. Es wird empfohlen, das zu vermeiden, z.b. durch
hintenanstellen eines `\`

## Einbinden von Seiten aus PDF Files (Wortsammler)

PDF-Seiten können direkt eingebunden werden. Dazu wird folgende
Markierung verwendet[^1]:

`~~PDF "filename" "eintrag für Inhaltsverzeichnis" level seitenbereich seitenvorschub~~`

Diese Eingabe muss allein in einer Zeile stehen und vier Leerzeichen
davor haben. Damit wird verhindert dass Wortsammler die Zeile umbricht,
und sie nicht mehr richtig verarbeitet werden kann.

Die letzten Parameter *level* (default=9) und *seitenbereich*
(default=alle) *seitenvorschub* (default="cleardoublepage") sind
optional.

Folgendes Beispiel bindet die Seite 3-4 aus der Datei
"`../Beispiel.pdf`" ein. Dabei wird ein PDF-Bookmark auf Ebene 2
erzeugt.

`~~PDF "../Beispiel.pdf" "das ist ein Beispielpdf" 2 3-4 clearpage~~`

## Einfügen von Textschnipseln (Wortsammler)

Textschnipsel werden in eigenen Schnipselbibbliotheken spezifiziert, die
in der Konfigurationsdatei referenziert werde.

Textschnipsel werden aufgerufen durch die Markierung

    ~~~~
    ~~SN <name>~~
    ~~~~

Dabei referenziert *name* den namen des Textschnipsels in der
Bausteindatei.

## Seitenumbrüche

Seitenumbrüche können durch das eingebettete LaTeX Kommando
`\clearpage`. Das funktioniert nur bei PDF-Ausgabe

## Tabellen

      Right     Left     Center     Default
    -------     ------ ----------   -------
         12     12        12            12
        123     123       123          123
          1     1          1             1

    Table:  Demonstration of simple table syntax.

For more complex tables, see the pandoc documentation in
[http://johnmacfarlane.net/pandoc/README.html\#pandocs-markdown][]

~~ED detail~~

## Horizontale Linien

Drei oder mehr Bindestriche, Sterne oder oder Unterstriche erzeugen eine
horizontale Linie

    ---
    * * *
    - - - -

~~ED all~~

## Zielgruppenspezifische Ausgaben (Editionen)

Wortsammler erlaubt zielgruppenspezifische Ausgaben. Dabei wird im
Textfluss die Verarbeitung ein- bz. ausgeschaltet, je nachdem welche
Zielgruppe adressiert wird.

Die Zielgruppen müssen im Manifest vordefiniert werden. Dann kann durch
folgende Markierung die Verarbeitung auf eine oder mehrere bestimmte
Zielgruppen geschaltet werden:

`~~ED <zielgruppe> [<zielgruppe>]*~~`

Dabei wird die Zielgruppe durch eine spezifische Zeichenkette
umgeschaltet (Durchstreichung), die auch in standard Markdown Programmen
eine sinnvolle Ausgabe liefert:

<!-- die quotes hier sind dafür da, dass die marke nicht verarbeitet wird.-->

> `~~ED intern` `extern~~` ab hier gilt: Text erscheint in Ausgabe
> `intern` als auch in `extern`.

-   Die Umschaltung wirkt ab einschliesslich der Zeile, die die
    Umschaltung enthält, bis zum Aufruf einer neuen Umschaltung.

-   Die möglichen Zielgruppen werden im <a href="#manifest">Manifest</a>
    festgelegt (Eintrag `:editions:`)

-   Eine durch Wortsammler vorgegebene Zielgruppe `all` erzeugt keine
    spezifische Ausgabe. Sie kennzeichnet vielmehr Inhalte, die in
    **allen** Ausgaben gleichermassen enthalten sind.

-   Bei einer Aufteilung auf mehrere Dateien wird empfohlen am Ende
    einer jeden Datei auf `all` zu schalten. Dadurch wird das System
    einfache wartbar.

-   Im Manifest kann auch eine Ausgabe mit der Eigenschaft "debug"
    spezifiziert werden. In dieser Ausgabe kommen alle Inhalte, die
    Zielgruppenumschaltung ist am Rand markiert.

## Hervorhebung von offenen Punkte "TODO"

In einer Debug-Ausgabe des Dokumentes werden Zeilen, die ein `todo:`
enthalten am Rand markiert, so dass man vorläufige Teile eines
Dokumentes gut erkennen kann.

~~ED all~~

[^1]: Die verwendung der Tilde führt bei normaler Vearbeitung zu einem
    durchgestrichenen Text

  [svn-checkout-menu]: ../RS_Process/images/svn-checkout-menu.jpg
  [svn-checkout-dialog]: ../RS_Process/images/svn-checkout-dialog.jpg
  [svn-icons]: ../RS_Process/images/svn-icons.jpg
  [svn-commit-menu]: ../RS_Process/images/svn-commit-menu.jpg
  [svn-commit-dialog]: ../RS_Process/images/svn-commit-dialog.jpg
  [svn-commit-result]: ../RS_Process/images/svn-commit-result.jpg
  [Pandoc]: http://johnmacfarlane.net/pandoc
  [LaTeX]: http://de.wikipedia.org/wiki/LaTeX
  [http://johnmacfarlane.net/pandoc/README.html\#pandocs-markdown]: http://johnmacfarlane.net/pandoc/README.html#pandocs-markdown
