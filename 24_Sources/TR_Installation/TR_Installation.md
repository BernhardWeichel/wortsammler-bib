# Installation

## Installation des vorbereiteten wortsammlerBib

Es gibt ein vorkonfiguriertes Paket für Wortsammler. Das ist ein
ZIP-Archiv, das ohne weitere Insallationsroutine auskommt. Es läuft auch
von einem USB-Stick. Inbetriebnahme in folgenden Schritten:

1.  Auspacken von `WortsammlerBib.zip` in ein Verzeichnis Ihrer Wahl
    (z.B. `c:\wortsammler`)

2.  Dos-Box öffnen

3.  aus Verzeichnis von Wortsammler

    `c:\wortsammler\wortsammlerBib\setWortsammler.bat` ausführen

4.  dann in das Tools-Verzeichnis des Projektes wechseln

## Installation mit Originalpaketen

### ruby

Bitte ruby ruby 1.8.7 verwenden.

-   windows
    -   download von [http://rubyinstaller.org/downloads/][]
    -   development kit installieren
        [DevKit-tdm-32-4.5.2-20111229-1559-sfx.exe][]

        Das braucht man nur, wenn man den Windows-Debugger verwenden
        muss. In den scripten ist rquire ruby-debug aukommentiert.

-   mac: OSX 10.8 bereits installiert

folgende Gems werden benötigt. Installation in einer DOS_Box /
Terminal:

-   gem install treetop
-   gem install nokogiri

### pandoc

-   Download [ttp://code.google.com/p/pandoc/downloads/list][]
-   Installation [http://rubyinstaller.org/downloads/][1]
-   Homepage [http://johnmacfarlane.net/pandoc/][]

### Tortoise Svn

-   [Tortoise SVN download][]

### Tortoise Git

-   ist eine Alternative zu Tortoise SVN
-   muss ich noch untersuchen

### TeX

-   mac: download [http://tug.org/mactex/][]

-   windows:

    -   [http://www.exomatik.net/U-Latex/USBTeXEnglish\#toc1][]
    -   webseite [http://www.miktex.org/2.9/setup][]

-   alternative auf windows

    -   [usbtex][]

### PDF-Viewer (Sumatra)

Dieser leichtgewichtige PDF-Viewer erlaubt es, das PDF-File während des
Betrachtens zu ändern.

-   Webseite kit Download:
    [http://blog.kowalczyk.info/software/sumatrapdf/free-pdf-reader-de.html][]

### MarkdownPad

-   Webseite [http://markdownpad.com/][]

-   alternative kommerzielles tool Mew

  [http://rubyinstaller.org/downloads/]: http://rubyforge.org/frs/%20download.php/76277/rubyinstaller-1.8.7-p370.exe
  [DevKit-tdm-32-4.5.2-20111229-1559-sfx.exe]: https://github.com/downloads/oneclick/rubyinstaller/DevKit-tdm-32-4.5.2-20111229-1559-sfx.exe
  [ttp://code.google.com/p/pandoc/downloads/list]: http://code.google.com/p/pandoc/downloads/list
  [1]: http://rubyinstaller.org/downloads/
  [http://johnmacfarlane.net/pandoc/]: http://johnmacfarlane.net/pandoc/
  [Tortoise SVN download]: http://tortoisesvn.net/downloads.html
  [http://tug.org/mactex/]: http://tug.org/mactex/
  [http://www.exomatik.net/U-Latex/USBTeXEnglish\#toc1]: http://www.exomatik.net/U-Latex/USBTeXEnglish#toc1
  [http://www.miktex.org/2.9/setup]: http://www.miktex.org/2.9/setup
  [usbtex]: http://www.exomatik.net/U-Latex/USBTeXEnglish
  [http://blog.kowalczyk.info/software/sumatrapdf/free-pdf-reader-de.html]:
    http://blog.kowalczyk.info/software/sumatrapdf/free-pdf-reader-de.html
  [http://markdownpad.com/]: http://markdownpad.com/
