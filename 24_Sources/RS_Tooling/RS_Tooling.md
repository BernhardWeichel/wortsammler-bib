# Anforderungen an Tooling

-   [RS_Tool_001] **Tooling soll ganzen Prozess abdecken** {

    -   Bearbeitung
    -   Editor
    -   Prüfung
    -   Beautifier
    -   Continuous Integration Server (sollte),
    -   Tracing checker
    -   Verarbeitung / Formatierer
    -   Versionierung
    -   Issue Tracker }(RS_Main_001, RS_Main_004)

-   [RS_Tool_002] **Tooling soll auf Windows und Mac laufen** { Das
    Pooling soll auf Windows und Mac laufen. Dabei soll ein gemeinsames
    Dateisystem verwendbar sein.

    Zu beachten

    -   Filenamensyntax
    -   Zeilenendebehandlung
    -   Scriptaufruf }(RS_Main_001, RS_Main_004)

Die folgenden Anforderungen gehen von einer Bearbeitung der
Textdokumente in Markdown aus.

-   Pro
    -   einfache Textdatei
    -   freie Tools verfügbar
    -   auch als Quelle gut lesbar

-   Contra
    -   Semantisch schwierig
    -   viele proprietäre Erweiterungen
    -   nicht sehr ausdrucksstark

Für eine einfache Projektorganisation wie hier betrachtet ist markdown
ausreichend.
