## Requirements on a Markdown Editor

### Use Cases

-   [UC_MdEd_001] **Edit documents** { }()

-   [UC_MdEd_002] **Collaborate on documents** { }()

-   [UC_MdEd_003] **Review Documents** { }()

-   [UC_MdEd_004] **Let the naive user work on documents** { }()

### Requirements

-   [RS_MdEd_001] **Platforms** { Should run on windows / mac
    }(UC_MdEd_002)

-   [RS_MdEd_002] **Markdown Syntax** { Shall support full markdown
    }(UC_MdEd_001)

-   [RS_MdEd_003] **Pandoc** { Shall support markdown extensions, in
    particular pandoc }(UC_MdEd_001)

-   [RS_MdEd_004] **Spellchecker** { Shall support spellchecker for

    -   English
    -   German

    }(UC_MdEd_001)

-   [RS_MdEd_005] **HTML preview** { Shall support HTML preview
    }(UC_MdEd_003)

-   [RS_MdEd_006] **Synchronized preview** { Should synchronize
    Preview and source text. This synchronization could also be
    implemented as a "double click" feature. }(UC_MdEd_003)

-   [RS_MdEd_007] **Customizable preview-Formatter** { Should support
    invocation of multiple previews such as HTML, PDF etc.

    This should be e.g. such that it applies to particular document or
    the entire installation. The following properties should be there:

    -   Styles to be applied
    -   command line for generating the preview-HTML. This would allow
        to add pandoc as previewer. It would even allow to do preview in
        other formats such as pdf.

    }(UC_MdEd_003)

-   [RS_MdEd_008] **Syntax coloring for snippets** { Should be
    configurable such that user defined patterns can be defined which
    are displayed in a particular style. A very prominent example is the
    representation of requirements as applied in this document
    }(UC_MdEd_001)

-   [RS_MdEd_009] **pretty print the markup source** { Should provide
    a pretty print of the markup source. In a multi author environment,
    this harmonizes the style of the source and allows easy comparison.

This could be implemented using a separate script. }(UC_MdEd_002)

-   [RS_MdEd_010] **Structure View** { Should provide a Structure view
    based on the headlines in the markup }(UC_MdEd_001)
