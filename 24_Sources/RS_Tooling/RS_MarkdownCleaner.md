## Anforderungen an Markdown - Cleaner

    TODO: Tagging

-   [RS_Mdc_001] **Soll einfache Lesbarkeit fördern** { Die einfache
    Lesbarkeit wird durch ein standardisierte Formatierung erreicht. Es
    soll die default-Ausgabe von Pandoc verwendet werden.}())

    [RS_Mdc_002] **Pandoc - Spezifika sollen unterstützt werden** {
    Pandoc hat einige Spezifika, die es bei der Ausgabe wiederum
    markdown-konform aufbereitet. Das erschwert aber die Lesbarkeit. Da
    wir zunächst nur Pandoc als Formatierer einsetzen, sollen diese
    Spezifika zurückgesetzt werden:

    -   Eingebettete Unterstriche werden von Pandoc mit "\\" geschützt.
        Dieser Schutz soll wieder entfernt werden. Das betrifft
        insbesondere die Anforderungsmarken (Requirement Tags), welche
        ja das "\_" verwenden.

    -   "-\>" soll zu "-\> werden"
    -   }()

-   [RS_Mdc_003] **Sonderzeichen sollen korrigiert werden** { Beim
    Kopieren von Inhalten aus Word bzw. PDF entstehen Sonderzeichen,
    welche später Probleme bereiten:

    -   Hochkomma
    -   Umlaute von Zweizeichendarstellung auf einzeichendarstellung in
        UTF}()

-   [RS_Mdc_004] **Links sollen als Reference-Links aufbereitet
    werden** {Dokumente werden einfacher zu pflegen, wenn Verweise als
    Reference Links ausgeführt werden (Link nur einmal definieren und
    mehrfach verwenden.)}()

-   [RS_Mdc_005] **Überschriften im ATX-Stil** {Markdown erlaubt zwei
    verschiedene Arten, die Überschriften auszuzeichnen. Der Cleaner
    soll alle Überschriften im ATX-Stil darstellen (Beginnend mit
    "\#")}()

-   [RS_Mdc_006] **Cleaner soll einfach anwendbar sein** { - Auf den
    ein Verzeichnis rekursiv (Files mit Endung `.md` und `.markdown`) -
    Auf eine einzelne Datei }()

-   [RS_Mdc_007] **Cleaner soll Backups anlegen** {Für den FehlerFall
    sollte der Cleaner sicherungskopien der bearbeiteten Dateien
    anlegen. Diese sollen die Endung `.md.bak.ttmmss` tragen. }()

-   [RS_Mdc_008] **Cleaner soll die Datei nur bei Bedarf ändern**
    {Dateien sollen nur geändert werden, wenn wirkliche Unterschiede
    auftreten. Im Fehlerfall darf der Cleaner die Dateien nicht
    ändern.}()

