# Demonstrator-Dokument für Requirements

## Dokumenthandhabung

[RS_DM_001] **Dokumentenformat** { Dokumente sollen in einem Format
erstellt werden, welches die Publikation auf verschiedenen Medien
erlaubt. Wesentliche Beispiele sind

-   PDF
-   HTML

}(PR_MAIN_003,PR_MAIN_004)

[RS_DM_002] **Versionierung** { Einzelnen Dokumentteile sollen unter
Versionskontrolle stehen.}(PR_MAIN_003,PR_MAIN_004)

[RS_DM_003] **Baselining** { Für das gesamte Handbuch soll ein
übergreifendes Baslining erfolge.}(PR_MAIN_003,PR_MAIN_004)

[RS_DM_004] **Zugriff auf Quellen** { Der Zugriff auf die
Dokuemntenquellen muss über Internet möglich sein. Siehe auch
[RS_DM_002].}()

[RS_DM_005] **Separate Publikation** { Dokumentenpflege und
Veröffentlichung soll getrennt werden.}()

## Dokumentbearbeitung

[RS_DM_006] **Tooling bei Teilnehmer** { Tooling sollte bei jedem
Teilnehmer installierbar sein}()

[RS_DM_007] **Tooling auf zentralem Server** { Tooling soll zentral
installiert sein. Damit wird die Referenz aufgesetzt und die finalen
Dokumente erstellt. Die Produktion sollte automatisiert ausgeführt
werden.}()

## Tracing

[RS_DM_008] **Repräsentation von Tracing** { Tracing zwischen Ebenen
soll wie folgt repräsentiert werden:

      [RS_DM_006] **Tooling auf zentralem Server**  { Tooling soll 
      zentral installiert sein. Damit wird die Referenz aufgesetzt 
      und die finalen Dokumente erstellt. Die Produktion sollte 
      automatisiert ausgeführt werden.}(PD_MAIN_001, PD_MAIN_003)

}()
