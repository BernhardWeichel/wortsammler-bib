# Experimente mit Tabellen

## zentierter text

  ------------------------
      **das bin ich**
        **und ich**
   **und ihh noch immer**
  ------------------------

hier ist ende

## Handhabung von Tabellen

  -----------------------------------------------------------------------
  Fruit                   Price                   Advantages
  ----------------------- ----------------------- -----------------------
  Bananas                 \$1.34                  - built-in wrapper -
                                                  bright color

  Oranges                 \$2.10                  - cures scurvy - tasty
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
  Fruit                   Price                   Advantages
  ----------------------- ----------------------- -----------------------
  Bananas                 \$1.34 built-in         wrapper built-in
                                                  wrapper built-in
                                                  wrapper built-in
                                                  wrapper built-in
                                                  wrapper built-in
                                                  wrapper built-in
                                                  wrapper built-in
                                                  wrapper built-in
                                                  wrapperbuilt-in wrapper
                                                  built-in wrapper
                                                  built-in wrapper
                                                  built-in wrapper
                                                  built-in wrapper
                                                  built-in wrapper
                                                  built-in wrapper
                                                  built-in wrapper
                                                  built-in
                                                  wrapperbuilt-in wrapper
                                                  built-in wrapper
                                                  built-in wrapper
                                                  built-in wrapper
                                                  built-in wrapper
                                                  built-in wrapper
                                                  built-in wrapper
                                                  built-in wrapper
                                                  built-in

  Oranges                 \$2.10                  - cures scurvy - tasty
  -----------------------------------------------------------------------

  ------------------------------------------------------------------------
  Trace   Title                satisfi f1       f2       f3       f4
                               ed by                              
  ------- -------------------- ------- -------- -------- -------- --------
  was ist Projekte sollen      das ist f1 und h f1 und h f1 und h das tzte
  das     erfolgreich          [RS_MA s e l n  d o c h  s |      
          durchgeführt werden  w c o s a n as   a n as            
          und noch so weiter   w c o s f1 a     f1 w              
                               a w c o ocwa n n owas was          
                               n       as | f u                   
                               IN_001 1                          
                               w s u d                            
                               n d n u                            
                               w n w w                            
                               w ]                                
  ------------------------------------------------------------------------

  -----------------------------------------------------------------------
  Fruit                   Price                   Advantages
  ----------------------- ----------------------- -----------------------
  Bananas                 \$1.34                  - built-in wrapper -
                                                  bright color

  Oranges                 \$2.10                  - cures scurvy - tasty
  -----------------------------------------------------------------------

# Points about Tweedledee and Tweedledum

Much has been made of the curious features of Tweedledee and Tweedledum.
We propose here to set some of the controversy to rest and to uproot all
of the more outlandish claims.

      .      Tweedledee     Tweedledum
  ---------- -------------- -----------------
     Age     14             14
    Height   3'2"           3'2"
   Politics  Conservative   Conservative
   Religion  "New Age"      Syrian Orthodox

  : T.-T. Data

# Mussolini's role in my downfall

  ------------------------------------------------------------------------
                *Drugs*          *Alcohol*            *Tobacco*
  ------------- ---------------- -------------------- --------------------
  Monday        3 Xanax          2 pints              3 cigars, 1 hr at
                                                      hookah bar

  Tuesday       14 Adderall      1 Boone's Farm, 2    1 packet Drum
                                 Thunderbird          

  Wednesday     2 aspirin        Tall glass water     (can't remember)
  ------------------------------------------------------------------------

  : *Tableau des vices*, deluxe edition

# Points about the facts

In recent years, more and more attention has been paid to opinion, less
and less to what were formerly called the cold, hard facts. In a spirit
of traditionalism, we propose to reverse the trend. Here are some of our
results.

  ----- ----- ----- -----
     12 12     12      12
    123 123    123    123
      1 1       1       1
  ----- ----- ----- -----

  : Crucial Statistics

# Recent innovations (1): False presentation

Some, moved by opinion and an irrational lust for novelty, would
introduce a non-factual element into the data, perhaps moving all the
facts to the left:

  ----- ----- ----- -----
  12    12    12    12
  123   123   123   123
  1     1     1     1
  ----- ----- ----- -----

  : Crucial "Statistics"

# Recent innovations (2): Illegitimate decoration

Others, preferring their facts to be *varnished*, as we might say, will
tend to 'label' the columns

  Variable   Before   During   After
  ---------- -------- -------- -------
  12         12       12       12
  123        123      123      123
  1000       1000     1000     1000

# Recent innovations (3): "Moderate" decoration

Or, maybe, to accompany this 'spin' with a centered or centrist
representation:

   Variable    Before  During   After
  ---------- -------- -------- -------
      12           12    12      12
     123          123   123      123
      1             1    1        1

# The real enemy

Some even accompany these representations with a bit of leftwing
clap-trap, suggesting the facts have drifted right:

  -------------------------------------------------------------------------
              Variable                    Before                  During Af
                                                                         t
                                                                         e
                                                                         r
  -------------------- ------------------------- ----------------------- --
                    12      12 -- Due to baleful                      12 12
                             bourgeois influence                         

                   123      123 -- Thanks to the                     123 12
                                renegade Kautsky                         3

                     1      1 -- All a matter of                       1 1
                          sound Party discipline                         
  -------------------------------------------------------------------------

  : *"The conditions are not ripe, comrades; they are **overripe**!"*

# The Truth

If comment be needed, let it be thus: the facts have drifted left.

  ----------------------------------------------------------------------
    Variable    Before             During             After
  ------------- ------------------ ------------------ ------------------
       12       12 (here's where   12 (due to lapse   12 (something to
                the rot set in )   of traditional     do with Clinton
                                   values)            and maybe the
                                                      '60's)

       123      123 (too much      123 (A=440?)       123
                strong drink)                         

        1       1                  1                  1 (Trilateral
                                                      Commission?)
  ----------------------------------------------------------------------

  : *The Decline of Western Civilization*

