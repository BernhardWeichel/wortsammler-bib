Attribute VB_Name = "InsertPoints"
Sub newProtokoll()
    Dim thisRow As Integer
    thisRow = ActiveCell.Row
    ActiveCell.EntireRow.Insert
    Cells(thisRow, 1).Value = Now()
    Cells(thisRow, 2).Value = 0
    Cells(thisRow, 3).Value = 0
    Cells(thisRow, 4).Value = ""
    Cells(thisRow, 5).Select
    Cells(thisRow, 10).Value = "Protokollant: Michael Reyer"
    Rows(thisRow).OutlineLevel = 1
    Cells(thisRow, 5).Value = Cells(2, 5).Value
End Sub

Sub newTopic()
    Dim thisRow As Integer
    thisRow = ActiveCell.Row
    If Cells(thisRow, 2).Value = 0 Or Cells(thisRow + 1, 4).Value = "" Then
            ActiveCell.Offset(1, 0).Select
    End If
    thisRow = ActiveCell.Row
    ActiveCell.EntireRow.Insert
    Cells(thisRow, 1).Value = Now()
    Cells(thisRow, 2).Value = 1
    Cells(thisRow, 4).Value = "T"
    Cells(thisRow, 5).Select
    renumberingRows (thisRow)
End Sub
Sub newInfo()
    newPoint ("I")
End Sub
Sub newToDo()
    newPoint ("A")
    Cells(ActiveCell.Row, 9).Value = "Offen"
End Sub
Sub newBeschluss()
    newPoint ("B")
End Sub
Sub newPoint(thisPoint As String)
    Dim thisRow As Integer
    thisRow = ActiveCell.Row

    If Not Cells(thisRow, 4).Value = "" Then
      ActiveCell.Offset(1, 0).Select
    End If
    thisRow = ActiveCell.Row
    If Cells(thisRow - 1, 2).Value = 0 Then
        Exit Sub
    End If
    ActiveCell.EntireRow.Insert
    Cells(thisRow, 1).Value = Now()
    Cells(thisRow, 2).Value = 1
    Cells(thisRow, 4).Value = thisPoint
    Cells(thisRow, 5).Select
    renumberingRows (thisRow)
End Sub
Sub renumberingRows(thisRow As Integer)
    While Cells(thisRow, 2).Value <> 0
        If Cells(thisRow, 4).Value = "T" Then
            Rows(thisRow).OutlineLevel = 2
            Cells(thisRow, 2).Value = Cells(thisRow - 1, 2).Value + 1
            Cells(thisRow, 3).Value = 1
        Else
            Rows(thisRow).OutlineLevel = 3
            Cells(thisRow, 2).Value = Cells(thisRow - 1, 2).Value
            Cells(thisRow, 3).Value = Cells(thisRow - 1, 3).Value + 1
        End If
        thisRow = thisRow + 1
    Wend
End Sub
Sub renumberingAll()
    Dim thisRow As Integer
    thisRow = ActiveCell.Row
    While Cells(thisRow, 1).Value <> 0
        Select Case Cells(thisRow, 4).Value
            Case ""
                Rows(thisRow).OutlineLevel = 1
                Cells(thisRow, 2).Value = 0
                Cells(thisRow, 3).Value = 0
            Case "T"
                Rows(thisRow).OutlineLevel = 2
                Cells(thisRow, 2).Value = Cells(thisRow - 1, 2).Value + 1
                Cells(thisRow, 3).Value = 1
            Case Else
                Rows(thisRow).OutlineLevel = 3
                Cells(thisRow, 2).Value = Cells(thisRow - 1, 2).Value
                Cells(thisRow, 3).Value = Cells(thisRow - 1, 3).Value + 1
        End Select
        thisRow = thisRow + 1
    Wend
End Sub

Sub reformatingSheet()
Cells.FormatConditions.Delete
    Columns("H:I").Select
    Selection.FormatConditions.Add Type:=xlExpression, Formula1:= _
        "=WENN($I1=""Offen"";WAHR;FALSCH)"
    Selection.FormatConditions(Selection.FormatConditions.Count).SetFirstPriority
    With Selection.FormatConditions(1).Interior
        .PatternColorIndex = xlAutomatic
        .Color = 13421823
        .TintAndShade = 0
    End With
    Selection.FormatConditions(1).StopIfTrue = True
    Columns("B:G").Select
    Selection.FormatConditions.Add Type:=xlExpression, Formula1:= _
        "=WENN($D1=""B"";WAHR;FALSCH)"
    Selection.FormatConditions(Selection.FormatConditions.Count).SetFirstPriority
    With Selection.FormatConditions(1).Interior
        .PatternColorIndex = xlAutomatic
        .Color = 15524100
        .TintAndShade = 0
    End With
    Selection.FormatConditions(1).StopIfTrue = True
    Columns("H:I").Select
    Selection.FormatConditions.Add Type:=xlExpression, Formula1:= _
        "=WENN(UND($I1=""Offen"";$H1<=HEUTE());WAHR;FALSCH)"
    Selection.FormatConditions(Selection.FormatConditions.Count).SetFirstPriority
    With Selection.FormatConditions(1).Font
        .Bold = True
        .Italic = False
        .TintAndShade = 0
    End With
    With Selection.FormatConditions(1).Interior
        .PatternColorIndex = xlAutomatic
        .Color = 255
        .TintAndShade = 0
    End With
    Selection.FormatConditions(1).StopIfTrue = True
    Columns("B:I").Select
    Selection.FormatConditions.Add Type:=xlExpression, Formula1:= _
        "=WENN($D1=""T"";WAHR;FALSCH)"
    Selection.FormatConditions(Selection.FormatConditions.Count).SetFirstPriority
    With Selection.FormatConditions(1).Interior
        .PatternColorIndex = xlAutomatic
        .Color = 65535
        .TintAndShade = 0
    End With
    Selection.FormatConditions(1).StopIfTrue = True
    Columns("A:K").Select
    Selection.FormatConditions.Add Type:=xlExpression, Formula1:= _
        "=WENN(UND($A1<>"""";ODER($B1=0;$B1=""0""));WAHR;FALSCH)"
    Selection.FormatConditions(Selection.FormatConditions.Count).SetFirstPriority
    'With Selection.FormatConditions(1).Font
    '    .Bold = True
    '    .Italic = False
    '    .TintAndShade = 0
    'End With
    With Selection.FormatConditions(1).Interior
        .PatternColorIndex = xlAutomatic
        .ThemeColor = xlThemeColorAccent6
        .TintAndShade = -0.249946592608417
    End With
    Selection.FormatConditions(1).StopIfTrue = True
    Cells(1, 1).Select
End Sub
