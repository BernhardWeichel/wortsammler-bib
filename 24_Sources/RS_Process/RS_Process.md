# Anforderungen (Requirements) an Projektabwicklung für NGO-Projekte

Dieses Dokument sammelt die Anforderungen an ein Projektabwicklungs- und
Managementsystem für gemeinnützige Organisationen.

## Hauptziele

![Übersicht][]

Folgende Hauptziele stehen im Fokus

-   [RS_Main_001] **Projekte sollen erfolgreich durchgeführt werden**
    { Projekte im gemeinnützigen Bereich sind oft mit verschiedenen,
    räumlich getrennten Beteiligten durchzuführen. Meist ist ein großes
    Engagement von ehren- und hauptamtlichen Mitarbeitern vorhanden.
    Dieses Engagement soll genutzt werden und die Projekte zu einem
    erfolgreichen Abschluss kommen. }()

-   [RS_Main_002] **Projektbeteiligte sollen motiviert werden** { Die
    Proejktbeteiligten sollen sich umfänglich einbringen können,
    Beiträge sollen aufgenommen und gewürdigt werden. }()

-   [RS_Main_003] **Jeder spielt nach den gleichen Regeln** { Der
    Stand des Projektes, insbesondere der zu erarbeitenden Ergebnisse
    soll für alle sichtbar und auch bearbeitbar sein. Rollen im Projekt
    sind klar benannt, Kommunikationswege sind bekannt. Kommunikation
    ist einfach.}()

-   [RS_Main_004] **Projektergebnisse sollen gesichert werden**
    {Erarbeitete Ergebnisse sollen einfliessen und nicht verloren gehen.
    Bearbeitungsaufträge sind klar definiert.}()

-   [RS_Main_005] **…**

Es ist offensichtlich, daß diese Ziele nicht nur von technischen
Maßnahmen abhängen. Trotzdem fokussiert dieses Meta-Projekt auf
technische Unterstützung.

## Anwendungsfälle und Projektphasen

**noch zu erarbeiten Siehe [Hauptziele][] …**

## Anforderungen an Dokumenthandhabung und Bearbeitung

-   [RS_DM_001] **Dokumentenformat** { Dokumente sollen in einem
    Format erstellt werden, welches die Zusammenstellung und Publikation
    auf verschiedenen Medien erlaubt. Wesentliche Beispiele sind

    -   PDF
    -   HTML

    }(RS_Main_001, RS_Main_003, RS_Main_004)

-   [RS_DM_002] **Versionierung** { Einzelnen Dokumentteile sollen
    unter Versionskontrolle stehen.}(RS_Main_003, RS_Main_004)

-   [RS_DM_003] **Baselining** { Für das gesamte Handbuch soll ein
    übergreifendes Baslining erfolgen. }(RS_Main_003, RS_Main_004)

-   [RS_DM_004] **Zugriff auf Quellen** { Der Zugriff auf die
    Dokuemntenquellen muss über Internet möglich sein. Siehe auch
    ->[RS_DM_002]. }(RS_Main_004)

-   [RS_DM_005] **Separate Publikation** { Dokumentenpflege und
    Veröffentlichung soll getrennt werden. }(RS_Main_004)

-   [RS_DM_006] **Tooling bei Teilnehmer** { Tooling sollte bei jedem
    Teilnehmer installierbar sein. }(RS_Main_003)

-   [RS_DM_007] **Tooling auf zentralem Server** { Tooling soll
    zentral installierbar sein. Damit wird die Referenz aufgesetzt und
    die finalen Dokumente erstellt. Die Produktion sollte automatisiert
    ausgeführt werden. }(RS_Main_003, RS_Main_004)

-   [RS_DM_008] **Definierte Verzeichnisstruktur** { Eine
    standardisierte Verzeichnisstruktur soll aufgesetzt werden.
    }(RS_Main_003, RS_Main_004)

-   [RS_DM_009] **Unterstützte Rechnerplatformen** { Die Bearbeitung
    der Dokumente sollte auf den Plattformen *Windows ab XP*, *Linux*
    und *Mac OSX* möglich sein. }(RS_Main_002, RS_Main_003)

-   [RS_DM_014] **Projektorganisation und Ergebnis-Erstellung soll
    flexibel sein** {
    -   Die Ablagen von Projektorganisation (kürzere Lebensdauer) und
        Ergebnisbearbeitung (längere Lebensdauer) soll getrennt sein.
    -   Die Ablage der Ergebnisbearbeitung solo in sich geschlossen
        sein, so sass man auch mit einer anderen Projektorganisation
        darauf arbeiten kann. }(RS_Main_003)

-   [RS_DM_010] **Projekte sollen in Modulen / Untergruppen
    organisiert werden können** {
    -   Die Ablage der Projektorganisation soll bei Bedarf in Module
        unterteilbar sein.
    -   Die Ablage der Einzelmodule soll alle Daten eines Moduls
        enthalten
    -   Übergreifende Informationen werden als eigenständiges Modul
        behandelt
    -   Beziehungen zwischen Modulen Sollen möglich sein.
        }(RS_Main_001, RS_Main_003)

-   [RS_DM_011] **Das System soll auf verschiedenen Rechnerplattformen
    verwendbar sein** {
    -   only 7bit ascii characters oder Unicode
    -   Case sensitive names
    -   keine Sonderzeichen in Dateinamen }(RS_Main_002,
        RS_Main_003)

-   [RS_DM_012] **Easy handling in communication** {
    -   nummerierte Verzeichnisse in der Arbeitsgruppenablage
    -   Dateinamen in der Ergebnisablage sollen einer Konvention folgen,
        welche die Rolle der Dokumente kennzeichnen. }(RS_Main_002,
        RS_Main_003)

## Anforderungen an Tracing

Bei der Verfolgung von Projektzielen kann ein Tracing eingesetzt werden.
Dieses ermöglicht die Darstellung, wie Ziele und Anforderungen umgesetzt
werden. Voraussetzung dafür ist dass diese überhaupt konkret erfasst
werden.

Im Grunde besteht tracing darin, dass Aussagen (Anforderungen,
Lösungsansätze usw.) konkrete erfasst, mit einer eindeutigen Kennung
versehen und zu einander in Beziehung gesetzt werden. In der Regel
ergibt sich daraus eine Hierarchie:

1.  Ziele
2.  Anforderungen
3.  Lösungsansätze

Tracing soll dabei immer nur von einer Ebene zur nächsten erfolgen. Zum
Beispiel leiten sich Anforderungen aus Zielen ab. Tracing ist also eine
Referenz von einer Anforderung zu den betroffenen Zielen. In anderen
Worten "Anforderungen tragen zu Zielen bei".

[RS_DM_013] **Repräsention von verfolgbaren Aussagen** { Tracing
zwischen Ebenen soll wie folgt repräsentiert werden:

![Repräsentation von verfolgbaren Aussagen][] 

}(RS_Main_001)

## Tracing in Mindmaps

Freemind kann verwendet werden um Requirements zu erfassen. Dabei wird
die Anforderung wie folgt beschrieben:

-   The specification Tag is in the text of the node.
-   You can have multiple lines in the text of the node uptrace
    references are one single line staring with "(" ending with ")". At
    best in the last line of the "note"
-   please use markdown syntax and do not use the formatting provided by
    the html-Editor in Freemind ->[RS_MAIN_001]

## Anforderungen an Tooling

Folgende Tools werden benötigt:

-   Editor
-   Versionsverwaltung
-   Dokumentenprozessor
-   Continuous Integration Server ?

Für diese Tools werden ggf. eigene Anforderungsdokumente erstellt.

## ..tbd..

  [Übersicht]: ../RS_Process/mindmap/ngopm.png
  [Hauptziele]: #hauptziele
  [Repräsentation von verfolgbaren Aussagen]: ../RS_Process/images/TraceInMou.png
