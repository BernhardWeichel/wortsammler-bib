
desc "compile the wortsammler manual"
task :manual do
	sh "ruby ../ZSUPP_Proo-lib/compileDocuments.rb ../CONF_Manifests/TR_Anleitung_Wortsammler"
end


desc "compile the wortsammler testfile"
task :testfile do
	sh "ruby ../ZSUPP_Proo-lib/compileDocuments.rb ../CONF_Manifests/TS_TestResults"
end

desc "compile the requirements document"
task :req do
	sh "ruby ../ZSUPP_Proo-lib/compileDocuments.rb ../CONF_Manifests/RS_Requirements_Ngopm"
end

desc "this help"
task :default do
	sh "rake -T"
end