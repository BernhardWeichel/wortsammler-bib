# intro

-	Das ist Gutemine. Sie hat die Aufgabe ein Handbuch für das Haus zusammenzustellen, welches sie verwaltet.

-	das ist Rainer Zufall - er unterstützt Gutemine  mit Beiträgen zu geordneten Abläufen.

-	und hier haben wir Guter Wohllaune - er leitet eine Arbeitsgruppe die dem Haus eine Seele geben soll.

-	Gutemine soll die Beiträge Ihrer Kollegen zusammenstellen.

- 	Ach da kommt eine Mail von Rainer - hallo Gutemine - hier mal ein erster Schuss. Kannst du das bitte mal einbauen

-	Gutemine ist begeistert und fängt sofort an. Es ist der erste Beitrag von Rainer Zufall und man soll die Leute ja nicht frustrieren.

- 	Aus der Mail entnimmt sie "handbuch.doc" und? ---  aach, was mach ich nun damit? ----  na gut ich schau es mir erst mal an.

-	Und hier kommt eine Mail G.Wohllaune - hallo Claudia, ich hab mal angefangen - gefällt dir das so?

-	Claudia ist irritiert aber neugierig und nimmt aus der Mail das Handbuch.doc. Sieht gut aus

- 	und sie macht bei Rainers Mail weiter … oops das ist doch das gleiche wie das von Günther.

- 	Während sie noch überlegt, kommt die Mail von Rainer: "Hallo Claudia ich hab noch ein paar Fehler korrigiert". Hier die neue Version von Handbuch.doc

-	Claudia ist langsam verwirrt über die vielen Handbuch.doc und entschliesst sich, Ordnung zu machen.

-	Sie nimmt also Rainers handbuch.doc und speichert sie unter handbuch-rainer.doc Günthers Datei speichert sie unter handbuch-günther.doc

-	Nun will sie die beiden zusammenführen und macht eine neue Datei Handbuch.doc und will dort handbuch-rainer.doc und handbuch-günter.doc reinkopieren. Aber die sehen ja ganz anders aus. Aber sie kennt sich mit Word aus und kann das gleich stellen.

-	Da kommt die mail vom großen Bruder: Hallo Claudia, kannst du das Handbuch in im Gemeindelayout machen und eine kompakte und eine Umfangreiche Ausgabe erstellen.

-	Klar kann ich das - schreibt sie und macht an handbuch.doc weiter. Sie fängt grad mit Handbuch-kompakt and - und ist schon ziemlich genervt, da kommt eine Mail von Rainer. Hallo Claudia, das von heute morgen war irgendwie im Delirium. Vergeiss es und nimm die alte Version.

-	Claudia läuft langsam rot an und schreibt dem großen Bruder: hallo wie soll das weiter gehen, ich kenne mich vor lauter mails nicht mehr aus, muss immer alles neu formatieren und jede Änderung extra reinblicken.


-	Das ist Ordana Hausmann - Sie hat die Aufgabe ein Handbuch für das Haus zusammenzustellen welche sie managt. 

-	Sie schreibt eine Liste, welche Teile darin enthalten sein sollen.

-	Hier macht sie für jedes Kapitel eine Datei und da ist nur die Überschrift drin.

- 	Hallo Rainer, hallo Guter, koch hab eure Beiträge mal vorbereitet, bitte schreibt einfach eure Texte da rein und und kümmert euch nicht drum, wie es aussieht.

-	Hoch motiviert fängt Rainer an. Er holt sich das Arbeitspaket und fängt an. Macht richtig Spass.

- 	Auch Guter ist ordentlich drauf und holt sich auch das Paket und fängt bei sich an.

-	Rainer ist schnell und gibt das Arbeitspaket zurück.

-	Guter möchte gerne wissen, was Rainer schreibt und und schaut mal nach, ob er schon was gemacht hat.

-	Ach ja, da ist es ja, ich kann direkt darauf Bezug nehmen.

- 	Ordana trinkt solange einen Kaffee - nein sie macht andere dringende Aufgaben.

-	Ordana ist mit ihren Aufgaben fertig und schaut, was ihre Jungs gemacht haben.

-	Da kommt der große Bruder - na, wie sieht es aus mit dem Handbuch

-	Ordana lässt WortSammler die Beiträge zusammenstellen und gibt ein schönes Buch an den großen Bruder.

-	Rainer ist noch was eingefallen und er holt sich das Arbeitspaket und baut noch den neuesten Geistesblitz ein.

- 	Der große Bruder will nun doch eine kompakte version.

-	Ordana gibt eine neue Liste an WortSammler und kriegt auch das hin.

-	Die letzten Änderungen von Rainer sind auch schon drin.

-	Der große Bruder will wissen, was nun eigentlich schon drin ist.

-	Der Hüter des Arbeitspaketes weiß auch das, er weiß bescheid über jede Änderung der kleinen Brüder

-	Claudia ist verzweifelt und fragt Ordana, wie sie so entspannt sein kann.






