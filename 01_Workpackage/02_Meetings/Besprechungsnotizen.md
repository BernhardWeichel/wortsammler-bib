# 2012-06-07 Bernhard und Dietrich Schult

- Ziele
	- Anforderung an Umgebung
	- 
- Anforderugen
- weiteres Vorgehen


#2012-09-13 Code Review mit Christian

## code Kosmetik
*    leerzeichen an "="
*    if not -> unless
*    ||= <=> [] if nil
*    

## Argument-handhabung

*    ARGV.length abprüfen ggf. usage zeile ausgeben
    Poor mans option parser
    
## Traceable - Architektur  
    
*    Traceable mit Klassenvariablen ist nicht elegant
*    LÖösungsansatz
    1. Traceable soll nur traceables handhaben
    2. TraceableSet soll die Traceables verwalten
        Methoden:
        * newFromMarkdown
        * newFromTex
        * << [TraceableSet]
        * …
    3. Traceable - umfang mit nil-Slot
    4. Klä#ren, ob Klassenvariablen global sind
    5. Traceable line 145 [nil, xxx].
    
## File handling

File.new(<string>).readlines  -> File.new erzeugt öffnet das File nicht

## reference tweaker

Abstrakte referenceTweaker und referencTewakerPdf bzw. referenceTweakerTex

private Methode prepareOneTraceReference(string)

in den konkreten Klassen ausformulieren.

    def prepareTraceReferences(string)
        string.gsub(/\s*/,"").split(",").map{|trace|
            
            itrace=mkInternalTraceId(trace)
            texTrace=mkTexTraceDisplay(trace)
            if @target=="pdf" then
                "\\hyperlink{#{itrace}}{#{texTrace}}"
                else
                "[#{trace}](\##{itrace})"
            end
        }.join(", ")
    end
    
wird dann zu

    def prepareTraceReferences(string)
        string.gsub(/\s*/,"").split(",").map{|trace|
            
            itrace=mkInternalTraceId(trace)
            texTrace=mkTexTraceDisplay(trace)
            prepareOneTraceReference(itrace, texTrace)
        }.join(", ")
    end    
 
# 2012-10-18 Modulleiter-Sitzung

## projektstatus

* Modul1 Projektorganisation
    * Prejekt ist begleitet
    * nun kommt noch die Kommunikation
    

* Modul2 
    * Übergabebsprechung zum Hausmeister
    * Handbuch wird erstellt
        * Inhaltsverzeichnis liegt vor
        * Dokumentation wird erstellt

* Modul 3 - übergabe an Verwaltung
    * Technikhandbuch ist inhaltlich erstellt
    * Digitialisierung steht noch aus Schuldt / Weichel
    
* Modul 4
    * Übergabe ist erfolgt
    * 
    
* Modul 5 
    * alle Themen abgearbeitet
    * Kommunikation -> Modul1
    
* Gesamtprojekt
    * Hohe Motivation - viel Zeiteinsatz
    * Nachhaltig kostendeckend, reibungsarber Betrieb bei Werterhaltender Nutzung
    * Auf
    
##  Bericht aus den Modulen

*  Hausverwaltung

    Teamrunde. Es blieben ein paar kleine Aufgaben (z.B. Windelentsorgung). Aber auch Dauerbrenner (Spontanbelegung).
    
    Auffällig: Dinge vereinbart, aber nicht umgesetzt, weil es unterschiedliche Sichten auf die Lösungen gibt. 
    Weiche Themen (Führung, Entscheidungsmsetzung / -revision)
    
* Technik

    noch 10 kleine punkte offen. 
    IT-Thema ist noch offen - Anforderungen

##    BG 

* Prämbel ist verabschiedet
    
* Mitglieder-Aufnahme im großen Saal



* Vergabebedingungen verabschiedet
    * Rabatt-Anpassungen
    * nur AWM - zwei kostenlose Nutzungen
    * keine dauerhafte Fremdvermietung
    
## Inhaltsverzeichnisse

* Verwaltung und Technik sind verabschiedet
*     



# BIS

* brüdergemeinderat
* Sitzungen, ein Eintrag pro sitzung - ist eine WEbseite - dokumente waren verlinkt

* Sitzungswebseite hat Link TAgesordnung und dort wird die TO eingestellt
* Dokumentenbibliothek mit Anlagen.


neu:

* Protokolle
* BGR Protokolle und Anlagen
* pro Jahr in einem File
* es gab einen Ordner Beschlüsse - lebt aber nicht mehr
* Ideen-Ordner - lebt auch nicht
* Infos - sowas wie Pressespiegel

* Grundsätliches

## Anforderungen

* Bereich für Controlling-Daten
* Workflow mit bearbeitern und Zugriffsrechte



    
    
        
    
 