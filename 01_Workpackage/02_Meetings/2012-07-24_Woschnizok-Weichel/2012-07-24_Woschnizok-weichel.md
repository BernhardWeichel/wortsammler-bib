# Gesprächsziel

- näheres / anderes Kennenlernen
- Information über Handbuch Gemeindehaus
- Mögliche Beiträge Bernhard Weichel
- Genereller Ansatz für Proejektorganisation (notwendig / sinnvoll)
- Synergien zu andern Vorhaben (AWM, Web13) ausloten
- Weiteres Vorgehen festlegen
- Sonstiges


# Agenda

- Stand Handbuch (10 Minuten)
- Projektunterstützung (30 Minuten)
- Weiteres Vorgehen (10 Minuten)
- Sonstiges (offen)


# Stand Handbuch Gemeindehaus (Thomas)

- Projektziele
- Organisation
- Zeitlicher Rahmen

# Überlegungen zu Projektunterstützung (Bernhard)

- Entwurf durchgehen
    - Ziele
    - Anforderungen an Projektunterstützung
    - Vorgehensweise (teile optional)
        - Anforderungsverfolgung (ggf. optional)
        - Verzeichnis-struktur
        - Dokumentenerstellung
     
- Demonstration der Vorgehensweise

    - Dokumentenerstellung
    
        [Beispiel aus progit](https://github.com/progit/progit/blob/master/de/02-git-basics/01-chapter2.markdown)
        
        [Pandoc](http://johnmacfarlane.net/pandoc/README.html)
        
        [eigene Beispiele](file:../../../24_Sources/ZGEN_Documents/)
    
    - Versionierung
    
    - Issue - Management
    
    - ggf. Demonstration an einem großen Projekt
    
        - [AUTOSAR](file://localhost/Volumes/Macintosh%20HD/Users/beweiche/beweiche_noTimeMachine/AUTOSAR-work/)
        
        - [Github](https://github.com/jgm/pandoc/issues) 
    

# weiteres Vorgehen (alle)

- Abstimmung mit weiteren Projektbeteiligten (ich konnte leider Dietrich Schuldt nicht mehr erreichen)?
- Mögliche Kosten
    - SVN - Hoster
    - …
     
- …




 
   