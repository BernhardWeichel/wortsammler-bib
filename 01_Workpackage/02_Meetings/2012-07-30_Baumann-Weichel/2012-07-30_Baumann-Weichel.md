# Themen

-   gegenseitige Vorstellung
-   Abriss über Hancbuchersllungsmethodik
    -   Erstellte Textdateien
    -   scripte zur Aufbereitung
    -   Verwaltungssystem GIT / Subversion / Github

# Zusammenfassung

-   Herr Baumann hält das vorgeschlagene System für tragfähig
-   Aus EDV - technischer Sicht ist es in der Brüdergemeinde anwendbar.
    Die notwendigen Installationen erscheinen nicht kritisch.
-   Herr Baumann könnte sich vorstellen, das auch für die Zusammenarbeit
    der Netzadministratoren zu verwenden. Dazu müsste es aber auf einem
    eigenen Server in der der Brüdergemeinde
-   Das Verfahren ist zukunftssicher
-   Es sollen keine personenbezogenen Daten abgelegt werden

# Weiteres Vorgehen

-   Rechner von Bernhard Weichel an das Gäste-Wlan bringen (Baumann)
-   Installationsanleitung für das Handbuch-Erstellsystem wird von
    Bernhard Weichel hocherstellt und an Herrn Baumann zum Review
    gegeben (Weichel)
-   Installation auf dem ersten Rechner machen Herr Baumann und Weichel
    gemeinsam.

